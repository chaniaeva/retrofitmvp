package com.example.retrofit.network

import com.example.retrofit.model.Pokemon
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("pokemon")
    fun getContentList(): Call<Pokemon>
}