package com.example.retrofit.model


import com.google.gson.annotations.SerializedName

data class Pokemon(
    @SerializedName("next")
    val next: String,
    @SerializedName("previous")
    val previous: String,
    @SerializedName("results")
    val results: List<Result>
)