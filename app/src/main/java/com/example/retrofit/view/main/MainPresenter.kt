package com.example.retrofit.view.main

import com.example.retrofit.model.Result

interface MainPresenter{
    fun success(pokemon: ArrayList<Result>)
    fun failed(message: String)
}