package com.example.retrofit.view.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.retrofit.R
import com.example.retrofit.model.Result
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainPresenter {
    private lateinit var pokemonAdapter: PokemonAdapter
    private lateinit var mainPresenterImplement: MainPresenterImplement

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainPresenterImplement = MainPresenterImplement(this@MainActivity)
        mainPresenterImplement.getContentList()

    }

    override fun success(pokemon: ArrayList<Result>) {
        Log.d("Berhasil", pokemon.toString())
        pokemonAdapter = PokemonAdapter(pokemon)
        rv.adapter = pokemonAdapter
        rv.apply {
            val linear = LinearLayoutManager(this@MainActivity)
            linear.orientation = LinearLayoutManager.VERTICAL
            rv.layoutManager = linear
        }
    }

    override fun failed(message: String) {
        Log.d("gagal", message)
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}