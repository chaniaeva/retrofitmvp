package com.example.retrofit.view.main

import android.util.Log
import com.example.retrofit.model.Pokemon
import com.example.retrofit.model.Result
import com.example.retrofit.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenterImplement(private val mainPresenter: MainPresenter) {
    fun getContentList(){
        ApiClient.instance.getContentList().enqueue(object : Callback<Pokemon> {
            override fun onResponse(call: Call<Pokemon>, response: Response<Pokemon>) {
                if(response.isSuccessful){
                    var pokemons = response.body()?.results as ArrayList<Result>
                    mainPresenter.success(pokemons)
                }
            }

            override fun onFailure(call: Call<Pokemon>, t: Throwable) {
                Log.d("errorResponse", t.localizedMessage)
                mainPresenter.failed(t.localizedMessage)
            }

        })
    }
}