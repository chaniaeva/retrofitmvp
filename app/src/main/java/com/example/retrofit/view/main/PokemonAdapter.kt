package com.example.retrofit.view.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.retrofit.R
import com.example.retrofit.model.Result
import kotlinx.android.synthetic.main.item_pokemon.view.*

class PokemonAdapter(var listPokemon: ArrayList<Result>): RecyclerView.Adapter<PokemonAdapter.ListPokemonHolder>() {

    inner class ListPokemonHolder(view: View): RecyclerView.ViewHolder(view) {
        val image = view.imagePokemon
        val name = view.namePokemon
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListPokemonHolder {
        return ListPokemonHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false))
    }

    override fun onBindViewHolder(holder: ListPokemonHolder, position: Int) {
        val content = listPokemon.get(position)

        content.let{
            Glide.with(holder.itemView).load(getImageUrl(it.url)).into(holder.image)
            holder.name.text = it.name
        }
    }

    fun getImageUrl(url:String): String {
        val index = url.split("/".toRegex()).dropLast(1).last()
        return "https://pokeres.bastionbot.org/images/pokemon/$index.png"
    }

    override fun getItemCount(): Int {
        return listPokemon.size
    }
}